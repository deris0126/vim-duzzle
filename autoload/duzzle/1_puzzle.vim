call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'hjkl',
  \ 'room' : [
  \   '|--------|',
  \   '|s       |',
  \   '|        |',
  \   '|        |',
  \   '|       g|',
  \   '|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'hjkl',
  \ 'room' : [
  \   '|--------|',
  \   '|s       |',
  \   '|------  |',
  \   '|        |',
  \   '|  ------|',
  \   '|       g|',
  \   '|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'hjkl',
  \ 'room' : [
  \   '|--------|',
  \   '|s       |',
  \   '|        |',
  \   '|--------|',
  \   '|        |',
  \   '|       g|',
  \   '|--------|',
  \ ],
  \ })

"count
call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'hjkl',
  \ 'room' : [
  \   '|--------|--------|',
  \   '|s       |        |',
  \   '|        |        |',
  \   '|--------|--------|',
  \   '|        |        |',
  \   '|        |       g|',
  \   '|--------|--------|',
  \ ],
  \ })

"count
call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'hjkl',
  \ 'room' : [
  \   '|--------|--------|--------|',
  \   '|s       |--------|        |',
  \   '|        |--------|        |',
  \   '|--------|--------|--------|',
  \   '|--------|        |        |',
  \   '|--------|       g|        |',
  \   '|--------|--------|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkw',
  \ 'room' : [
  \   '|--------|',
  \   '|s       |',
  \   '|        |',
  \   '|        |',
  \   '|       g|',
  \   '|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkwb',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|',
  \   '|s      p|',
  \   '|------  |',
  \   '|p       |',
  \   '|  ------|',
  \   '|       g|',
  \   '|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : ['j','k','w','b','e','ge'],
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|',
  \   '|s     pp|',
  \   '|------- |',
  \   '|pp      |',
  \   '|- ------|',
  \   '|       g|',
  \   '|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkfF',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|',
  \   '|s      p|',
  \   '|------- |',
  \   '|p       |',
  \   '| -------|',
  \   '|       g|',
  \   '|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkfFtT',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|',
  \   '|s      p|',
  \   '|------ -|',
  \   '|p       |',
  \   '|- ------|',
  \   '|       g|',
  \   '|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkfF;',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|----------------|',
  \   '|s     p  p  p  p|',
  \   '|--------------- |',
  \   '|p  p  p  p      |',
  \   '| ---------------|',
  \   '|      p  p  p  g|',
  \   '|----------------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkf;,',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|----------------|',
  \   '|s     p  p  p  p|',
  \   '|--------------- |---|',
  \   '|p  p  p  p        p |',
  \   '| ---------------|---|',
  \   '|      p  p  p  g|',
  \   '|----------------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkfFtT',
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s       |            |       p|',
  \   '|--------|            |------- |',
  \   '                      |p       |',
  \   '             |--------|- ------|',
  \   '             |g       |        |',
  \   '             |--------|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : ['j','k','w','b','e','ge'],
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s       |            |      pp|',
  \   '|--------|            |------ -|',
  \   '                      |pp      |',
  \   '             |--------| -------|',
  \   '             |g       |        |',
  \   '             |--------|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jk^$',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '#|--------|',
  \   '#|s       |',
  \   '#|--------|',
  \   '#',
  \   '##########g',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : ['j','k','^','0','$','g_'],
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|---------|',
  \   ' |s       |',
  \   ' |--------|',
  \   ' |',
  \   ' |-----------|',
  \   '          g   ',
  \   '-------------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : ['j','k','^','0','$','g_'],
  \ 'room' : [
  \   '|---------| ',
  \   ' |s       | ',
  \   ' |--------| ',
  \   ' |',
  \   '----------|g',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkfF}{',
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s       |            |        |',
  \   '|--------|            |--------|',
  \   '',
  \   '|--------|            |--------|',
  \   '|        |            |       p|',
  \   '|--------|            |--------|',
  \   '',
  \   '  -------|            |--------|',
  \   '  |      |            |       p|',
  \   '|--------|            |--------|',
  \   '',
  \   '             |--------|--------|',
  \   '             |g       |        |',
  \   '             |--------|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkfF}{',
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '  |      |            |       p|',
  \   '  -------|            |--------|',
  \   '',
  \   '|--------|            |--------|',
  \   '|        |            |        |',
  \   '|--------|            |--------|',
  \   '',
  \   '|--------|            |--------|',
  \   '|s       |            |        |',
  \   '|--------|            |--------|',
  \   '',
  \   '             |--------|--------|',
  \   '             |g       |       p|',
  \   '             |--------|--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : '/',
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s       |            |       g|',
  \   '|--------|            |--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : '?',
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|g       |            |       s|',
  \   '|--------|            |--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jk*wnN',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s      p|            |p      p|',
  \   '|--------|            |--------|',
  \   '',
  \   '|--------|            |--------|',
  \   '|       p|            |p      p|',
  \   '|--------|            |--------|',
  \   '',
  \   '|--------|            |--------|',
  \   '|       p|            |p      g|',
  \   '|--------|            |--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jk#wnN',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|       p|            |p      g|',
  \   '|--------|            |--------|',
  \   '',
  \   '|--------|            |--------|',
  \   '|       p|            |p      p|',
  \   '|--------|            |--------|',
  \   '',
  \   '|--------|            |--------|',
  \   '|s      p|            |p      p|',
  \   '|--------|            |--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkhl%',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s     { |            | }     g|',
  \   '|--------|            |--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkhl%',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s     ( |            | )      |',
  \   '|--------|            |        |',
  \   '|                     |        |',
  \   '|--------|            |        |',
  \   '|      [ |            | ]      |',
  \   '|        |            |--------|',
  \   '|        |',
  \   '|        |            |--------|',
  \   '|      ( |            | )     g|',
  \   '|--------|            |--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jkhl%',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s     ( |            |        |',
  \   '|--------|            |        |',
  \   '|                     |        |',
  \   '|--------|            |        |',
  \   '|      ( |            | (     g|',
  \   '|        |            |--------|',
  \   '|        |',
  \   '|        |            |--------|',
  \   '|      ) |            | )    ) |',
  \   '|--------|            |--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : 'jk%fF',
  \ 'disable_key_count' : 1,
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s     ( |            |        |',
  \   '|--------|            |        |',
  \   '|                     |        |',
  \   '|--------|            |        |',
  \   '|        |            |       g|',
  \   '|        |            |--------|',
  \   '|        |',
  \   '|        |            |--------|',
  \   '|      p |            |      ) |',
  \   '|--------|            |--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : {
  \   'n' : 'jkhld',
  \   'o' : 'f',
  \ },
  \ 'disable_key_count' : 1,
  \ 'options' : [
  \   'setlocal modifiable',
  \ ],
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s       |            | }     g|',
  \   '|--------|            |--------|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : {
  \   'n' : 'jkhld',
  \   'o' : 't',
  \ },
  \ 'disable_key_count' : 1,
  \ 'options' : [
  \   'setlocal modifiable',
  \ ],
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s      p|            |        |',
  \   '|--   ---|            |        |',
  \   '  |---|               |------ g|',
  \   '                            |--|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : {
  \   'n' : 'jkhld',
  \   'o' : 't',
  \ },
  \ 'disable_key_count' : 1,
  \ 'options' : [
  \   'setlocal modifiable',
  \ ],
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s       |            |        |',
  \   '|--------|            |-----  g|',
  \   '                           |---|',
  \ ],
  \ })

call duzzle#add_puzzle('_', {
  \ 'name' : 'first room',
  \ 'enable_keys' : {
  \   'n' : 'jkhld',
  \   'o' : 't',
  \ },
  \ 'limit_key' : {
  \   'h' : 1,
  \   'j' : 1,
  \   'k' : 1,
  \   'l' : 1,
  \ },
  \ 'disable_key_count' : 1,
  \ 'options' : [
  \   'setlocal modifiable',
  \ ],
  \ 'room' : [
  \   '|--------|            |--------|',
  \   '|s       |            |        |',
  \   '|--------|            |------ g|',
  \   '                            |--|',
  \ ],
  \ })

